<?php

	namespace PhpOffice\PhpWord\Writer\HTML\Element;

	use PhpOffice\PhpWord\Element\AbstractContainer;
	use PhpOffice\PhpWord\Shared\Converter;
	use PhpOffice\PhpWord\Style;

	class BulletList extends Container
	{
		/**
		 * Write container
		 *
		 * @return string
		 */
		public function write()
		{
			/** @var \PhpOffice\PhpWord\Element\BulletList $container */
			$container = $this->element;
			if (!$container instanceof AbstractContainer) {
				return '';
			}
			$content = '';
			$elements = $container->getElements();
			foreach ($elements as $element) {
				$containerClass = substr(get_class($element), strrpos(get_class($element), '\\') + 1);
				$withoutP = in_array($containerClass, ['Text', 'TextRun', 'Link']) ? true : false;
				$elementClass = get_class($element);
				$writerClass = str_replace('PhpOffice\\PhpWord\\Element', $this->namespace, $elementClass);
				if (class_exists($writerClass)) {
					/** @var \PhpOffice\PhpWord\Writer\HTML\Element\AbstractElement $writer Type hint */
					$writer = new $writerClass($this->parentWriter, $element, $withoutP);
					$content .= $writer->write();
				}
			}

			/**
			 * @var $style \PhpOffice\PhpWord\Style\ListItem
			 */
			$style = $container->getStyle();

			$numStyleName = $style->getNumStyle();
			$styles = Style::getStyles();
			/** @var Style\NumberingLevel $totalStyle */
			$totalStyle = &$styles[$numStyleName]->getLevels()[(int)$container->getDepth()];

			if ($totalStyle === null) {
				$totalStyle = array_values($styles[$numStyleName]->getLevels())[0];
			}

			$currentDepth = (int)$container->getDepth();
			if ($currentDepth > $this->lastDepth) {
				$totalStyle->setCurrentValue(null);
			}

			for ($i = $container->getDepth(); $i >= 0; $i--) {
				if ($i == $container->getDepth()) {
					$totalStyle->setCurrentValue($totalStyle->getCurrentValue() + 1);
				}
			}

			$hanging = Converter::twipToPixel($totalStyle->getHanging());

			$parentParagraphStyle = $this->element->getParagraphStyle();
			$parentAlignment = $parentParagraphStyle->getAlignment() ?: 'left';
			$parentSpacing = $parentParagraphStyle->getSpace();
			if (!is_null($parentSpacing)) {
				$before = $parentSpacing->getBefore();
				$after = $parentSpacing->getAfter();
				$css['margin-top'] = !is_null($before) ? ($before / 20).'pt' : '0';
				$css['margin-bottom'] = !is_null($after) ? ($after / 20).'pt' : '0';
			} else {
				$css['margin-top'] = '0';
				$css['margin-bottom'] = '0';
			}

			return "<ul style='text-align: $parentAlignment;margin-top: {$css['margin-top']};margin-bottom: {$css['margin-bottom']};'>$content</ul>";
		}
	}