<?php

namespace PhpOffice\PhpWord\Writer\HTML\Element;

use PhpOffice\PhpWord\Element\AbstractContainer;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Style;
use PhpOffice\PhpWord\Style\Paragraph;
use PhpOffice\PhpWord\Writer\HTML\Style\Paragraph as ParagraphStyleWriter;

class ListItemRun extends Container
{
    /**
     * Write container
     *
     * @return string
     */
    public function write()
    {
        /** @var \PhpOffice\PhpWord\Element\ListItemRun $container */
        $container = $this->element;
        if (!$container instanceof AbstractContainer) {
            return '';
        }
        $content = '';
        $elements = $container->getElements();
        foreach ($elements as $element) {
            $containerClass = substr(get_class($element), strrpos(get_class($element), '\\') + 1);
            $withoutP = in_array($containerClass, ['Text', 'TextRun', 'Link']) ? true : false;
            $elementClass = get_class($element);
            $writerClass = str_replace('PhpOffice\\PhpWord\\Element', $this->namespace, $elementClass);
            if (class_exists($writerClass)) {
                /** @var \PhpOffice\PhpWord\Writer\HTML\Element\AbstractElement $writer Type hint */
                $writer = new $writerClass($this->parentWriter, $element, $withoutP);
                $content .= $writer->write();
            }
        }

        /**
         * @var $style \PhpOffice\PhpWord\Style\ListItem
         */
        $style = $container->getStyle();

        $numStyleName = $style->getNumStyle();
        $styles = Style::getStyles();
        /** @var Style\NumberingLevel $totalStyle */
        $totalStyle = &$styles[$numStyleName]->getLevels()[(int)$container->getDepth()];

        if ($totalStyle === null) {
            $totalStyle = array_values($styles[$numStyleName]->getLevels())[0];
        }

        $currentDepth = (int)$container->getDepth();
        if ($currentDepth > $this->lastDepth) {
            $totalStyle->setCurrentValue(null);
        }

        for ($i = $container->getDepth(); $i >= 0; $i--) {
            if ($i == $container->getDepth()) {
                $totalStyle->setCurrentValue($totalStyle->getCurrentValue() + 1);
            }
        }

				$style = $this->getParagraphStyle();

        return "<li{$style}>$content</li>";
    }

	/**
	 * Write paragraph style
	 *
	 * @param $style
	 * @return string
	 */
	private function getParagraphStyle($styles = null)
	{
		$styles = $styles === null ? $this->getParagraphStyleArray() : $styles;
		if (empty($styles)) {
			return '';
		}

		$inlines = '';

		foreach ($styles as $style) {
			$inlines .= " {$style['attribute']}=\"{$style['style']}\"";
		}

		return $inlines;
	}

	private function getParagraphStyleArray()
	{

		/** @var \PhpOffice\PhpWord\Element\Text $element Type hint */
		$element = $this->element;
		$style = [];
		if (!method_exists($element, 'getParagraphStyle')) {
			return $style;
		}

		$paragraphStyle = $element->getParagraphStyle();
		$pStyleIsObject = ($paragraphStyle instanceof Paragraph);
		if ($pStyleIsObject) {
			$styleWriter = new ParagraphStyleWriter($paragraphStyle);
			$style['style'] = $styleWriter->write();
		} elseif (is_string($paragraphStyle)) {
			$style['style'] = $paragraphStyle;
		}
		if (!empty($style)) {
			$attribute = $pStyleIsObject ? 'style' : 'class';
			$style['attribute'] = $attribute;
		}

		return array_filter([$style]);
	}

}