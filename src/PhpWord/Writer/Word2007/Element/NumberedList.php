<?php
	/**
	 * This file is part of PHPWord - A pure PHP library for reading and writing
	 * word processing documents.
	 *
	 * PHPWord is free software distributed under the terms of the GNU Lesser
	 * General Public License version 3 as published by the Free Software Foundation.
	 *
	 * For the full copyright and license information, please read the LICENSE
	 * file that was distributed with this source code. For the full list of
	 * contributors, visit https://github.com/PHPOffice/PHPWord/contributors.
	 *
	 * @see         https://github.com/PHPOffice/PHPWord
	 * @copyright   2010-2018 PHPWord contributors
	 * @license     http://www.gnu.org/licenses/lgpl.txt LGPL version 3
	 */

	namespace PhpOffice\PhpWord\Writer\Word2007\Element;

	use PhpOffice\PhpWord\Writer\Word2007\Style\Paragraph as ParagraphStyleWriter;

	/**
	 * ListItemRun element writer
	 *
	 * @since 0.10.0
	 */
	class NumberedList extends AbstractElement
	{
		/**
		 * Namespace; Can't use __NAMESPACE__ in inherited class (ODText)
		 *
		 * @var string
		 */
		protected $namespace = 'PhpOffice\\PhpWord\\Writer\\Word2007\\Element';

		/**
		 * Write list item element.
		 */
		public function write()
		{
			$xmlWriter = $this->getXmlWriter();
			$container = $this->getElement();
			if (!$container instanceof \PhpOffice\PhpWord\Element\NumberedList) {
				return;
			}
			$thisContainerId = rand(0, 1000); // random list id
			$elements = $container->getElements();

			foreach ($elements as $element) {
				$elementClass = substr(get_class($element), strrpos(get_class($element), '\\') + 1);
				$withoutP = in_array($elementClass, ['Text', 'TextRun', 'Link']);
				$writerClass = $this->namespace . '\\' . $elementClass;
				if ($elementClass == 'ListItemRun' || $elementClass == 'ListItem')
				{
					$element->getStyle()->setNumId($thisContainerId);
				}

				if (class_exists($writerClass)) {
					/** @var \PhpOffice\PhpWord\Writer\Word2007\Element\AbstractElement $writer Type hint */
					$writer = new $writerClass($xmlWriter, $element, $withoutP);
					$writer->write();
				}
			}
		}
	}
